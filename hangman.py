# Derek Li & Crawford Marks
# Hang-man Project
# November 30th 2016
import random
randomChoice = 0
def wordslist():
    global randomChoice
    fileopen = open("wordsforhangman.txt" , "rt") # open file
    wordlist = fileopen.readlines()# read file
    rangeoflines = len (wordlist)# the number of lines in lists
    randomChoice = random.randint(1, rangeoflines) #it selects a random number
    print(wordlist[randomChoice]) # randomly select the words from the list
    fileopen.close()
class Hangman():
    def __init__(self):
        print ("Welcome to 'Hangman', are you ready to play?")
        print ("(1)Yes, I am ready")
        print ("(2)No, I do not want to play it..")
        user_choice_1 = input("->")
        if user_choice_1 == '1': # leading users to the game
            self.start_game()
        elif user_choice_1 == '2': # ending the game
                print("Ending")
        exit()
    def start_game(self):
        self.core_game()
    def core_game(self):
        global randomChoice
        guesses = 0
        letters_used = ""
        the_word = randomChoice # assign the variable to the word
        progress = len(randomChoice)
        print(progress)
        while guesses < 6:
            guess = input("Guess a letter!")
            if guess in the_word and guess not in letters_used:
                print ("right guess: guesses = {}".format(guesses))
                print ("Oh yes, your guess was RIGHT!")
                letters_used += "," + guess
                self.hangman_graphic(guesses)
                print ("Progress: " + self.progress_updater(guess, the_word, progress))
                print ("Letter used: " + letters_used)
            elif guess not in the_word and guess  not in letters_used:
                guesses += 1
                print ("that guess was WRONG!")
                print ("I am so sorry!")
                print ("Keep going?")
                letters_used +=  guess
                print(type(guesses))
                print(letters_used)
                self.hangman_graphic(guesses)
                print ("Progress: " + "".join(progress))
                print ("Letter used: " + letters_used)
                print ("wrong guess: guesses = {}".format(guesses))
            else:
                print ("That's the wrong letter...")
                print ("Try again!")
        return guess
    def hangman_graphic(self, guesses):
        int(guesses)
        print("used guesses".format(guesses))
        if guesses == 0:
            print ("________      ")
            print ("|      |      ")
            print ("|             ")
            print ("|             ")
            print ("|             ")
            print ("|             ")
        elif guesses == 1:
            print ("________      ")
            print ("|      |      ")
            print ("|      0      ")
            print ("|             ")
            print ("|             ")
            print ("|             ")
        elif guesses == 2:
            print ("________      ")
            print ("|      |      ")
            print ("|      0      ")
            print ("|     /       ")
            print ("|             ")
            print ("|             ")
        elif guesses == 3:
            print ("________      ")
            print ("|      |      ")
            print ("|      0      ")
            print ("|     /|      ")
            print ("|             ")
            print ("|             ")
        elif guesses == 4:
            print ("________      ")
            print ("|      |      ")
            print ("|      0      ")
            print ("|     /|\     ")
            print ("|             ")
            print ("|             ")
        elif guesses == 5:
            print ("________      ")
            print ("|      |      ")
            print ("|      0      ")
            print ("|     /|\     ")
            print ("|     /       ")
            print ("|             ")
        else:
            print ("________      ")
            print ("|      |      ")
            print ("|      0      ")
            print ("|     /|\     ")
            print ("|     / \     ")
            print ("|             ")
            print ("The noose tightens around your neck, and you feel the")
            print ("sudden urge to urinate.")
            print ("GAME OVER!")
    def progress_updater(self, guess, the_word, progress):
        i = 0
        while i < len(the_word):
            if guess == the_word[i]:
                progress[i] = guess
                i += 1
            else:
                i += 1
        return "".join(progress)
wordslist()
game = Hangman()